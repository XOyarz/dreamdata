FROM python:3.8-slim-buster

ENV PYTHONUNBUFFERED 1
RUN adduser --home /home/dream --uid 1000 dream
WORKDIR /mail_app

COPY requirements.txt requirements.txt
RUN apt-get update && \
    apt-get install -y curl && \
    pip install --upgrade pip && \
    pip install -r requirements.txt

COPY ./mail_app /mail_app

RUN chown -R dream:dream /mail_app
USER dream

ENV PYTHONPATH=/mail_app

#ENTRYPOINT ["python", "main.py", "worker", "-l", "info"]
#ENTRYPOINT [ "uvicorn", "main:app", "--reload"]
CMD uvicorn main:app --host 0.0.0.0 --port 8081
