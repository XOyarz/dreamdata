from service_manager.mailgun_client import MailGun
from service_manager.sendgrid_client import SendGrid
from service_manager.exceptions import MailGunException, SendGridException, MailClientException


class MailClient:
    def __init__(self):
        self.primary = MailGun()
        self.secondary = SendGrid()

    def send(self, mail):
        try:
            self.primary.send(mail)
        except MailGunException:
            self.secondary.send(mail)
        except SendGridException:
            raise MailClientException
