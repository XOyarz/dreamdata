import requests
import os
from service_manager.exceptions import MailGunException


class MailGun:
    def __init__(self):
        self.api_key = os.environ.get("MAILGUN_API_KEY")
        self.mailgun_url = os.environ.get("MAILGUN_URL")

    @staticmethod
    def generate_email(mail):
        return {
            "from": mail.sender,
            "to": mail.receiver,
            "subject": mail.subject,
            "text": mail.message
        }

    def send(self, mail):
        try:
            response = requests.post(
                url=self.mailgun_url,
                auth=("api", self.api_key),
                data=self.generate_email(mail)
            )
            response.raise_for_status()
        except requests.exceptions.HTTPError as err:
            raise MailGunException(err)
