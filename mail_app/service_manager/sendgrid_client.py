import os
from sendgrid import SendGridAPIClient
from sendgrid.helpers.mail import Mail
from service_manager.exceptions import SendGridException
from python_http_client.exceptions import UnauthorizedError, ForbiddenError


class SendGrid(SendGridAPIClient):
    def __init__(self):
        self.client = SendGridAPIClient(os.environ.get('SENDGRID_API_KEY'))

    @staticmethod
    def generate_email(mail):
        return Mail(
            from_email=mail.sender,
            to_emails=mail.receiver,
            subject=mail.subject,
            html_content=mail.message
        )

    def send(self, mail):
        try:
            message = self.generate_email(mail)
            response = self.client.send(message)

            if response.status_code != 202:
                raise SendGridException(response.body)
        except UnauthorizedError as err:
            raise SendGridException(err)
        except ForbiddenError as err:
            raise SendGridException(err)
