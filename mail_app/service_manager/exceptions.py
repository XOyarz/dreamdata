class MailClientException(Exception):
    pass


class MailGunException(MailClientException):
    pass


class SendGridException(MailClientException):
    pass
