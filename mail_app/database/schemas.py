from typing import List, Optional

from pydantic import BaseModel


class EmailBase(BaseModel):
    subject: Optional[str] = None
    message: str
    receiver: str


class EmailCreate(EmailBase):
    pass


class EmailToSend(EmailBase):
    sender: str


class Email(EmailBase):
    id: int
    sender_id: int

    class Config:
        orm_mode = True


class UserBase(BaseModel):
    username: str
    email: str


class UserCreate(UserBase):
    password: str


class User(UserBase):
    id: int
    emails_sent: List[Email] = []

    class Config:
        orm_mode = True


class UserInDB(User):
    hashed_password: str


class Token(BaseModel):
    access_token: str
    token_type: str


class TokenData(BaseModel):
    username: Optional[str] = None