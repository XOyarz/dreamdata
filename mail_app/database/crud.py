from sqlalchemy.orm import Session
from sqlalchemy.exc import IntegrityError
from fastapi import HTTPException
from database.models import User, Email
from database.schemas import UserCreate, EmailCreate
from passlib.context import CryptContext


pwd_context = CryptContext(schemes=["bcrypt"], deprecated="auto")

def get_user(db: Session, user_id: int):
    return db.query(User).filter(User.id == user_id).first()


def get_users(db: Session, skip: int = 0, limit: int = 100):
    return db.query(User).offset(skip).limit(limit).all()


def get_user_by_username(db: Session, username: str):
    return db.query(User).filter(User.username == username).first()


def get_password_hash(password):
    return pwd_context.hash(password)


def create_user(db: Session, user: UserCreate):
    try:
        hashed_password = get_password_hash(user.password)
        db_user = User(email=user.email, username=user.username, hashed_password=hashed_password)
        db.add(db_user)
        db.commit()
        db.refresh(db_user)
        return db_user
    except IntegrityError:
        raise HTTPException(status_code=400, detail="Email or Username is already in use. Try another one!")


def get_emails(user_id: int, db: Session, skip: int = 0, limit: int = 100):
    return db.query(Email).filter(Email.sender_id == user_id).offset(skip).limit(limit).all()


def create_user_email(db: Session, email: EmailCreate, sender_id: int):
    db_email = Email(**email.dict(), sender_id=sender_id)
    db.add(db_email)
    db.commit()
    db.refresh(db_email)
    return db_email