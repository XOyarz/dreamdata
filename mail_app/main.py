from email_validator import validate_email, EmailNotValidError
from python_http_client.exceptions import BadRequestsError
from typing import List
from fastapi import Depends, FastAPI, HTTPException, status, Response
from fastapi.responses import RedirectResponse

from sqlalchemy.orm import Session
from database.crud import create_user, get_users, create_user_email
from database.models import Base
from database.schemas import User, UserCreate, UserBase, EmailCreate, EmailToSend, EmailBase
from database.database import SessionLocal, engine

from service_manager.manager import MailClient
from authentication.utils import get_current_active_user

from database.schemas import Token
from datetime import timedelta
from fastapi.security import OAuth2PasswordBearer, OAuth2PasswordRequestForm
from authentication.utils import authenticate_user, create_access_token
from service_manager.exceptions import MailClientException

Base.metadata.create_all(bind=engine)


app = FastAPI()

# Dependency - move to db
def get_db():
    db = SessionLocal()
    try:
        yield db
    finally:
        db.close()


@app.get("/")
async def root():
    return RedirectResponse(url='/docs')


@app.post("/user/", response_model=User)
def post_user(user: UserCreate, db: Session = Depends(get_db)):
    return create_user(db=db, user=user)


@app.get("/users/", response_model=List[UserBase])
def read_users(skip: int = 0, limit: int = 100, db: Session = Depends(get_db)):
    users = get_users(db, skip=skip, limit=limit)
    users_public = []
    for user in users:
        users_public.append(UserBase(username=user.username, email=user.email))
    return users_public


@app.post("/send-emails/")
def send_email(
        response: Response,
        email: EmailCreate,
        db: Session = Depends(get_db),
        current_user: User = Depends(get_current_active_user)
):
    try:
        validate_email(email.receiver)
        to_mail = EmailToSend(**email.dict(), sender=current_user.email)
        client = MailClient()
        client.send(to_mail)
        create_user_email(db=db, email=email, sender_id=current_user.id)
        return {"status": "Your message has been sent!"}

    except MailClientException:
        response.status_code = status.HTTP_503_SERVICE_UNAVAILABLE
        return {"status": "Unfortunately your email could not be sent."}
    except EmailNotValidError as err:
        return {"error": str(err)}
    except BadRequestsError as err:
        return {"error": str(err)}


@app.get("/users/emails/", response_model=List[EmailBase])
def read_users_email(current_user: User = Depends(get_current_active_user)):
    emails = current_user.emails_sent
    my_emails = []
    for email in emails:
        my_emails.append(EmailBase(subject=email.subject, message=email.message, receiver=email.receiver))
    return my_emails


@app.get("/users/me/", response_model=UserBase)
async def read_users_me(current_user: User = Depends(get_current_active_user)):
    return UserBase(username=current_user.username, email=current_user.email)


@app.post("/token", response_model=Token)
async def login_for_access_token(db: Session = Depends(get_db), form_data: OAuth2PasswordRequestForm = Depends()):
    user = authenticate_user(db=db, username=form_data.username, password=form_data.password)
    if not user:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Incorrect username or password",
            headers={"WWW-Authenticate": "Bearer"},
        )
    #     access_token_expires = timedelta(minutes=ACCESS_TOKEN_EXPIRE_MINUTES)
    access_token_expires = timedelta(minutes=60)
    access_token = create_access_token(
        data={"sub": user.username}, expires_delta=access_token_expires
    )
    return {"access_token": access_token, "token_type": "bearer"}